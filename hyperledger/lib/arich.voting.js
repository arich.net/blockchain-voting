'use strict';
/**
 * Make Votation Transaction
 * @param {arich.voting.makeVotation} tx.
 * @transaction
 */

async function execMakeVotation(makeVotation) {
  var citizen = makeVotation.citizen;
  var candidate = makeVotation.candidate;
  var poll = makeVotation.poll;
  var vote = getFactory().newConcept('arich.voting', 'Vote');
  var date = new Date();
  var finishdate = new Date(poll.finishDate);
  if (date < finishdate) {
    return query('selectUserVotedPolls', { userId: citizen.userId, pollId: poll.pollId }).then(function(assets) {
      var cVotedAlready = false;
      assets.forEach(function(asset) {
        cVotedAlready = true;
      });
      if (! cVotedAlready) {
        vote.voteDate = date;
        vote.candidate = candidate;
        poll.votes.push(vote);
        return getAssetRegistry('arich.voting.VotePoll').then(function(assetRegistry) {
          return assetRegistry.update(poll);
        }).then(function(){
          return getParticipantRegistry('arich.voting.Citizen').then(function(participantRegistry) {
              var votedpoll = getFactory().newConcept('arich.voting', 'VotedPoll');
              votedpoll.pollId = poll.pollId;
              votedpoll.votedAlready = true;
              citizen.votedPolls.push(votedpoll);
              return participantRegistry.update(citizen);
          });
        });
      }
    });
  }
  else {
    var event = getFactory().newEvent('arich.voting', 'VoteEvent');
    event.message = "Vote later than Finish Date";
    emit(event);
  }
}

