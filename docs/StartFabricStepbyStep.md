INSTALL FABRIC FROM ZERO
========================
* Cleanup everything
```
$ ./stopFabric.sh
$ ./teardownFabric.sh
$ ./teardownAllDocker.sh
$ for card in `composer card list | egrep arich-voting | awk '($1 !~ /Connection/) {print $2}'`; do composer card delete -c $card; done
$ rm -rf ~/.composer
$ docker rmi $(docker images -q)
```
* Uninstall all composer environment
```
$ npm uninstall -g composer-cli composer-rest-server generator-hyperledger-composer
$ npm uninstall -g composer-playground
```
* Install npm
```
$ nvm —-version
$ nvm install v8
$ nvm use 8
$ node --version
```

* Make sure we have the latest fabric dev tools
```
$ mkdir ~/fabric-dev-servers && cd ~/fabric-dev-servers
$ curl -O https://raw.githubusercontent.com/hyperledger/composer-tools/master/packages/fabric-dev-servers/fabric-dev-servers.tar.gz
$ tar -xvf fabric-dev-servers.tar.gz
$ cd ~/fabric-dev-servers
$ export FABRIC_VERSION=hlfv12
```

* Install composer environment
```
$ npm install -g composer-cli@0.20 composer-rest-server@0.20 generator-hyperledger-composer@0.20
$ npm install -g composer-playground
```
* Start Up2Dated Infrastructure
```
$ ./downloadFabric.sh
$ ./startFabric.sh
```
* Create the admin card, take a note of the output
```
$ ./createPeerAdminCard.sh
```
>NOTE 1: 
>* Verify docker containers
>```
>$ docker ps
>```
>* Get logs from docker containers
>```
>$ docker logs -f $CONTAINER_NAME
>```

* Create connection.json
```json
{
    "name": "arich-voting",
    "x-type": "hlfv1",
    "version": "1.0.0",
    "peers": {
        "peer0.org1.example.com": {
            "url": "grpc://192.168.130.14:7051",
            "eventUrl": "grpc://192.168.130.14:7053"
        }
    },
    "certificateAuthorities": {
        "ca.org1.example.com": {
            "url": "http://192.168.130.14:7054",
            "caName": "ca.org1.example.com"
        }
    },
    "orderers": {
        "orderer.example.com": {
            "url": "grpc://192.168.130.14:7050"
        }
    },
    "organizations": {
        "Org1": {
            "mspid": "Org1MSP",
            "peers": [
                "peer0.org1.example.com"
            ],
            "certificateAuthorities": [
                "ca.org1.example.com"
            ]
        }
    },
    "channels": {
        "composerchannel": {
            "orderers": [
                "orderer.example.com"
            ],
            "peers": {
                "peer0.org1.example.com": {
                    "endorsingPeer": true,
                    "chaincodeQuery": true,
                    "eventSource": true
                }
            }
        }
    },
    "client": {
        "organization": "Org1",
        "connection": {
            "timeout": {
                "peer": {
                    "endorser": "300",
                    "eventHub": "300",
                    "eventReg": "300"
                },
                "orderer": "300"
            }
        }
    }
}
```

* Create PeerAdmin card 
```
$ ln -s ~/fabric-dev-servers/fabric-scripts/hlfv12/composer/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp/keystore/114aab0e76bf0c78308f89efc4b8c9423e31568da0c340ca187a9b17aa9a4457_sk .
$ ln -s ~/fabric-dev-servers/fabric-scripts/hlfv12/composer/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp/admincerts/Admin@org1.example.com-cert.pem .
$ composer card create -p connection.json \
    -u PeerAdmin -c Admin@org1.example.com-cert.pem \
    -k 114aab0e76bf0c78308f89efc4b8c9423e31568da0c340ca187a9b17aa9a4457_sk \
    -r PeerAdmin -r ChannelAdmin
```
* Import Business Netowrk Admin Card into composer cards
```
$ composer card import -f PeerAdmin@arich-voting--network.card
```
>NOTE 2:
>* To check imported cards
>```
>$ composer card list 
>```
* Configure NPM on container peer0.org1.example.com to not verify SSL connections due we go through a Proxy. We crate the following nmprc file
```
$ cat /opt/hyperledger/git/RealHackersCSHackaton/npmrc.docker 
; userconfig /opt/hyperledger/.npmrc
strict-ssl = false
```
* Create business network definition BNA
```
$ composer archive create --sourceType dir --sourceName .
```
* Install Composer Business Network with PeerAdmin card
```
$ composer network install -c PeerAdmin@arich-voting--network \
    -a arich-voting@`jq .version < package.json | sed -e s/\"//g`.bna \
    -o npmrcFile=/opt/hyperledger/git/blockchain-voting/npmrc.docker
```
* Start Composer Business Network and create Admin Card
```
$ composer network start --networkName arich-voting \
    --networkVersion `jq .version < package.json | sed -e s/\"//g` \
    -A admin -S adminpw -c PeerAdmin@arich-voting--network
```
* Import admin of network business card
```
$ composer card import -f admin@arich-voting.card 
```
* Verify business network is correctly installed
```
$ composer network ping -c admin@arich-voting
```
Configure composer-rest on docker with GitHub OAuth
===================================================
* Create card for REST admin account
```
$ composer participant add -c admin@arich-voting \
   -d '{
      "$class":"org.hyperledger.composer.system.NetworkAdmin", 
      "participantId":"restadmin"
   }'
$ composer identity issue -c admin@arich-voting -f restadmin.card \
   -u restadmin -a "resource:org.hyperledger.composer.system.NetworkAdmin#restadmin"
$ composer card import -f restadmin.card
```
* Start mongo docker interface to store states using loopback-connector
```
$ docker run -d --name mongo --network composer_default -p 27017:27017 mongo
```
* Check the following DockerFile
```
$ cat Dockerfile 
FROM hyperledger/composer-rest-server
RUN npm install --production loopback-connector-mongodb passport-github && \
    npm install --production passport-google-oauth2 && \
    npm install base64-js ieee754 && \
    npm cache clean --force && \
    ln -s node_modules .node_modules
```
* Build docker image for composer-rest
```
$ docker build -t myorg/my-composer-rest-server .
```
* Check the corresponding environment variables 
```
$ cat envvars.txt
COMPOSER_CARD=restadmin@arich-voting
COMPOSER_NAMESPACES=never
COMPOSER_AUTHENTICATION=true
COMPOSER_MULTIUSER=true
COMPOSER_PROVIDERS='{
    "github": {
        "provider": "github",
        "module": "passport-github",
        "clientID": "******************",
        "clientSecret": "********************************",
        "authPath": "/auth/github",
        "callbackURL": "https://blockchain.arich-net.com/auth/github/callback",
        "successRedirect": "http://blockchain.arich-net.com/Success.htm",
        "failureRedirect": "http://blockchain.arich-net.com/Fail.htm"
    },
    "google": {
        "provider": "google",
        "module": "passport-google-oauth2",
        "clientID": ""******************",",
        "clientSecret": "********************************",
        "authPath": "/auth/google",
        "callbackURL": "/auth/google/callback",
        "scope": "https://www.googleapis.com/auth/plus.login",
        "successRedirect": "/",
        "failureRedirect": "/"
    }
}'
COMPOSER_DATASOURCES='{
    "db": {
        "name": "db",
        "connector": "mongodb",
        "host": "mongo"
    }
}'
```
* Set environment variables and test is working
```
$ source envvars.txt
$ echo $COMPOSER_CARD
restadmin@arich-voting
```
* Make ~/.composer read/write for ALL
```
$ chmod -R 777 ~/.composer
```
* Change connection settings for rest admin composer card
```
$ cat $HOME/.composer/cards/restadmin@arich-voting/connection.json \
    | sed -e 's/127.0.0.1/192.168.0.3/g' > /tmp/connection.json \
    && cp -p /tmp/connection.json $HOME/.composer/cards/restadmin@arich-voting/
```
* Run docker image my-composer-rest-server
```
$ docker run \
     -d \
     -e COMPOSER_CARD=${COMPOSER_CARD} \
     -e COMPOSER_NAMESPACES=${COMPOSER_NAMESPACES} \
     -e COMPOSER_AUTHENTICATION=${COMPOSER_AUTHENTICATION} \
     -e COMPOSER_MULTIUSER=${COMPOSER_MULTIUSER} \
     -e COMPOSER_PROVIDERS="${COMPOSER_PROVIDERS}" \
     -e COMPOSER_DATASOURCES="${COMPOSER_DATASOURCES}" \
     -e NODE_TLS_REJECT_UNAUTHORIZED="0" \
     -v ~/.composer:/home/composer/.composer \
     --name rest \
     --network composer_default \
     -p 3000:3000 \
     myorg/my-composer-rest-server
```
* Verify docker rest logs
```
$ docker logs -f rest
```
* Add Bank participants
```
$ composer participant add -c admin@arich-voting \
   -d '{
      "$class": "arich.voting.Bank",
      "bankId": "bankid0001",
      "name": "Credit Suisse"
   }'
$ composer participant add -c admin@arich-voting \
   -d '{
      "$class": "arich.voting.Bank",
      "bankId": "bankid0002",
      "name": "UBS"
   }'
```
* Add User participants
```
# Ariel Vasquez
$ composer participant add -c admin@arich-voting \
   -d '{
      "$class": "arich.voting.User",
      "userId": "arich-net",
      "email": "arich.net@gmail.com",
      "firstName": "Ariel",
      "lastName": "Vasquez",
      "userGroup": "peerAdmin"
   }'
$ composer identity issue -u arich-net -a arich.voting.User#arich-net -c admin@arich-voting 
$ composer card import -f arich-net@arich-voting.card
# Fabian Diergart
$ composer participant add -c admin@arich-voting \
   -d '{
      "$class": "arich.voting.User",
      "userId": "FabianDi",
      "email": "fdiergardt@gmx.de",
      "firstName": "Fabian",
      "lastName": "Diergart",
      "userGroup": "peerAdmin",
      "bank": "arich.voting.Bank#bankid0001"
   }'
$ composer identity issue -u FabianDi -a arich.voting.User#FabianDi -c admin@arich-voting 
$ composer card import -f FabianDi@arich-voting.card
# Ghosh Aniruddha
$ composer participant add -c admin@arich-voting \
   -d '{
      "$class": "arich.voting.User",
      "userId": "ani-gh-22",
      "email": "ronyzurich@gmail.com",
      "firstName": "Aniruddha",
      "lastName": "Ghosh",
      "userGroup": "peerUser",
      "bank": "arich.voting.Bank#bankid0001"
   }'
$ composer identity issue -u ani-gh-22 -a arich.voting.User#ani-gh-22 -c admin@arich-voting 
$ composer card import -f ani-gh-22@arich-voting.card
```
* Test one of the participants
```
$ composer network ping -c arich-net@arich-voting
The connection to the network was successfully tested: arich-voting
        Business network version: 0.0.1
        Composer runtime version: 0.19.1
        participant: arich.voting.User#arich-net
        identity: org.hyperledger.composer.system.Identity#4e7dec33a837511146c58631fd07e0becf909f19e599d6bfb2fcaa416e09a3cc
```
>NOTE 3: Sometimes there are updates that may affect your business network. Please try to refresh any docker image on your side
>* List docker images
>```
>$ docker images
>```
>* Remove image
>```
>$ docker rmi $IMAGE_ID
>```
>* Remove container (Useful when image contains childs)
>```
>$ docker rm $CONTAINER_NAME
>```
* Export the cards before importing them into the wallets
```
$ composer card export -c arich-net@arich-voting -f arich-net@arich-voting.card
$ composer card export -c FabianDi@arich-voting -f FabianDi@arich-voting.card
$ composer card export -c ani-gh-22@arich-voting -f ani-gh-22@arich-voting.card
```
* Make ~/.composer read/write for ALL
```
$ chmod -R 777 ~/.composer
```
* Import credential files into wallets using explorer (api/wallet/import)
* Start playground and publish it on 8080
```
docker run --name composer-playground --publish 8080:8080 --detach hyperledger/composer-playground
```


